@startuml
	class Music
	class Artist
	class Album

Music o-- "1" Album

Album o-- "1" Artist
@enduml