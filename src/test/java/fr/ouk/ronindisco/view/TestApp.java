package fr.ouk.ronindisco.view;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import fr.ouk.ronindisco.model.Album;
import fr.ouk.ronindisco.model.Artist;
import fr.ouk.ronindisco.viewmodel.DiscoViewModel;
import javafx.application.Platform;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

@ExtendWith(ApplicationExtension.class)
class TestApp extends AppView{
	
	@Start
	void init(Stage s) {
		try {
			start(s);
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	void verifyArtistAdd(FxRobot robot) {
		robot.clickOn("#menuAdd");
		robot.clickOn("#menuAddArtist");
		robot.clickOn("#nameText");
		robot.write("Ailee");
		robot.clickOn("#confirm");
		assertEquals("Ailee", DiscoViewModel.getInstance().getListA().get(0).getName());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
	}
	@Test
	void verifyAlbumAdd(FxRobot robot) {
		DiscoViewModel.getInstance().getImpl().insertArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
		//album add
		robot.clickOn("#menuAdd");
		robot.clickOn("#menuAddAlbum");
		robot.clickOn("#nameText");
		robot.write("AMY");
		//select combobox
		Platform.runLater(()->{
			ComboBox<?> actualComboBox = robot.lookup("#artistList").query();
			actualComboBox.getSelectionModel().selectFirst();
		});
		robot.clickOn("#confirm");
		assertEquals("Ailee", DiscoViewModel.getInstance().getListA().get(0).getName());
		assertEquals("AMY", DiscoViewModel.getInstance().getListAm().get(0).getName());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
	}
	@Test
	void verifyMusicAdd(FxRobot robot) {
		DiscoViewModel.getInstance().getImpl().insertArtist("Ailee");
		DiscoViewModel.getInstance().getImpl().insertAlbum("AMY", new Artist("Ailee"));
		DiscoViewModel.getInstance().updateAllList();
		robot.clickOn("#menuAdd");
		robot.clickOn("#menuAddMusic");
		robot.clickOn("#nameText");
		robot.write("Dont teach me");
		//select combobox
		Platform.runLater(()->{
			ComboBox<?> actualComboBox = robot.lookup("#artistList").query();
			actualComboBox.getSelectionModel().selectFirst();
			ComboBox<?> actualComboBox2 = robot.lookup("#albumList").query();
			actualComboBox2.getSelectionModel().selectFirst();
		});
		robot.clickOn("#confirm");
		assertEquals("Ailee", DiscoViewModel.getInstance().getListA().get(0).getName());
		assertEquals("AMY", DiscoViewModel.getInstance().getListAm().get(0).getName());
		assertEquals("Dont teach me", DiscoViewModel.getInstance().getListM().get(0).getName());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
	}
	@Test
	void verifyArtistModify(FxRobot robot) {
		DiscoViewModel.getInstance().getImpl().insertArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
		robot.clickOn("#tabArtist");
		robot.clickOn("Ailee");
		robot.clickOn("#modify");
		robot.clickOn("#field");
		robot.write("Bewhy");
		robot.clickOn("#confirm");
		assertEquals("Bewhy", DiscoViewModel.getInstance().getListA().get(0).getName());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Bewhy");
		DiscoViewModel.getInstance().updateAllList();
	}
	@Test
	void verifyAlbumModify(FxRobot robot) {
		DiscoViewModel.getInstance().getImpl().insertArtist("Ailee");
		DiscoViewModel.getInstance().getImpl().insertAlbum("AMY", new Artist("Ailee"));
		DiscoViewModel.getInstance().updateAllList();
		robot.clickOn("#tabAlbum");
		robot.clickOn("AMY");
		robot.clickOn("#modify");
		robot.clickOn("#field");
		robot.write("Luv");
		robot.clickOn("#confirm");
		assertEquals("Luv", DiscoViewModel.getInstance().getListAm().get(0).getName());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
	}
	@Test
	void verifyMusicModify(FxRobot robot) {
		DiscoViewModel.getInstance().getImpl().insertArtist("Ailee");
		DiscoViewModel.getInstance().getImpl().insertAlbum("AMY", new Artist("Ailee"));
		DiscoViewModel.getInstance().getImpl().insertMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		DiscoViewModel.getInstance().updateAllList();
		robot.clickOn("#tabMusic");
		robot.clickOn("Dont teach me");
		robot.clickOn("#modify");
		robot.clickOn("#field");
		robot.write("Blind");
		robot.clickOn("#confirm");
		assertEquals("Blind", DiscoViewModel.getInstance().getListM().get(0).getName());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
	}
	@Test
	void verifyArtistDelete(FxRobot robot) {
		DiscoViewModel.getInstance().getImpl().insertArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
		robot.clickOn("#tabArtist");
		robot.clickOn("Ailee");
		robot.clickOn("#delete");
		robot.clickOn("#confirm");
		assertEquals(true, DiscoViewModel.getInstance().getListA().isEmpty());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
	}
	@Test
	void verifyAlbumDelete(FxRobot robot) {
		DiscoViewModel.getInstance().getImpl().insertArtist("Ailee");
		DiscoViewModel.getInstance().getImpl().insertAlbum("AMY", new Artist("Ailee"));
		DiscoViewModel.getInstance().updateAllList();
		robot.clickOn("#tabAlbum");
		robot.clickOn("AMY");
		robot.clickOn("#delete");
		robot.clickOn("#confirm");
		assertEquals(true, DiscoViewModel.getInstance().getListAm().isEmpty());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
	}
	@Test
	void verifyMusicDelete(FxRobot robot) {
		DiscoViewModel.getInstance().getImpl().insertArtist("Ailee");
		DiscoViewModel.getInstance().getImpl().insertAlbum("AMY", new Artist("Ailee"));
		DiscoViewModel.getInstance().getImpl().insertMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		DiscoViewModel.getInstance().updateAllList();
		robot.clickOn("#tabMusic");
		robot.clickOn("Dont teach me");
		robot.clickOn("#delete");
		robot.clickOn("#confirm");
		assertEquals(true, DiscoViewModel.getInstance().getListM().isEmpty());
		DiscoViewModel.getInstance().getImpl().deleteArtist("Ailee");
		DiscoViewModel.getInstance().updateAllList();
	}
	@AfterEach
	void afterEach() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
