package fr.ouk.ronindisco.database;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.ouk.ronindisco.model.Album;
import fr.ouk.ronindisco.model.Artist;
import fr.ouk.ronindisco.model.Music;

class SQLiteImplTest {
	private static SQLiteJDBC connect = new SQLiteJDBC();
	private static SQLiteImpl impl = new SQLiteImpl(connect);
	
	@BeforeAll
	static void connect() {
		connect.connect("bddTest");
		impl.createTable();
	}
	@AfterAll
	static void disconnect() {
		connect.closeConnect();
	}


	@Test
	void testInsertArtist() {
		impl.insertArtist("Ailee");
		assertEquals("Ailee", impl.readArtist("Ailee").getName());
		impl.deleteArtist("Ailee");
	}

	@Test
	void testUpdateArtist() {
		impl.insertArtist("Ailee");
		assertEquals("Ailee", impl.readArtist("Ailee").getName());
		impl.updateArtist("JpTheWavy",new Artist("Ailee"));
		assertEquals("JpTheWavy", impl.readArtist("JpTheWavy").getName());
		impl.deleteArtist("JpTheWavy");
	}

	@Test
	void testUpdateAlbum() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		assertEquals("Ailee", impl.readAlbum("AMY", new Artist("Ailee")).getArtist().getName());
		assertEquals("AMY", impl.readAlbum("AMY", new Artist("Ailee")).getName());
		impl.updateAlbum("New Empire", new Album("AMY", new Artist("Ailee")));
		assertEquals("New Empire", impl.readAlbum("New Empire", new Artist("Ailee")).getName());
		impl.deleteAlbum("New Empire",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

	@Test
	void testInsertAlbum() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		assertEquals("Ailee", impl.readAlbum("AMY", new Artist("Ailee")).getArtist().getName());
		assertEquals("AMY", impl.readAlbum("AMY", new Artist("Ailee")).getName());
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

	@Test
	void testInsertMusic() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		impl.insertMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		assertEquals("Dont teach me", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getName());
		assertEquals("AMY", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getAlbum().getName());
		assertEquals("Ailee", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getAlbum().getArtist().getName());
		impl.deleteMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

	@Test
	void testUpdateMusic() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		impl.insertMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		assertEquals("Dont teach me", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getName());
		assertEquals("AMY", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getAlbum().getName());
		assertEquals("Ailee", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getAlbum().getArtist().getName());
		impl.updateMusic("New Ego",new Music("Dont teach me",new Album("AMY", new Artist("Ailee"))));
		assertEquals("New Ego", impl.readMusic("New Ego",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getName());
		impl.deleteMusic("New Ego", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

	@Test
	void testReadArtist() {
		impl.insertArtist("Ailee");
		assertEquals("Ailee", impl.readArtist("Ailee").getName());
		impl.deleteArtist("Ailee");
	}

	@Test
	void testReadAlbum() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		assertEquals("Ailee", impl.readAlbum("AMY", new Artist("Ailee")).getArtist().getName());
		assertEquals("AMY", impl.readAlbum("AMY", new Artist("Ailee")).getName());
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

	@Test
	void testReadMusic() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		impl.insertMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		assertEquals("Dont teach me", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getName());
		assertEquals("AMY", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getAlbum().getName());
		assertEquals("Ailee", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getAlbum().getArtist().getName());
		impl.deleteMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

	@Test
	void testReadAllArtist() {
		impl.insertArtist("Ailee");
		impl.insertArtist("JpTheWavy");
		impl.insertArtist("BeWhy");
		assertEquals("Ailee", impl.readAllArtist().get(0).getName());
		assertEquals("JpTheWavy", impl.readAllArtist().get(1).getName());
		assertEquals("BeWhy", impl.readAllArtist().get(2).getName());
		impl.deleteArtist("Ailee");
		impl.deleteArtist("JpTheWavy");
		impl.deleteArtist("BeWhy");
	}

	@Test
	void testReadAllAlbum() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		impl.insertAlbum("Im",new Artist("Ailee"));
		assertEquals("AMY", impl.readAllAlbum().get(0).getName());
		assertEquals("Im", impl.readAllAlbum().get(1).getName());
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteAlbum("Im",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
		
	}

	@Test
	void testReadAllMusic() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		impl.insertMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		impl.insertMusic("New Ego", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		assertEquals("Dont teach me", impl.readAllMusic().get(0).getName());
		assertEquals("New Ego", impl.readAllMusic().get(1).getName());
		impl.deleteMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		impl.deleteMusic("New Ego", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

	@Test
	void testDeleteArtist() {
		impl.insertArtist("Ailee");
		assertEquals("Ailee", impl.readArtist("Ailee").getName());
		impl.deleteArtist("Ailee");
		assertEquals(null, impl.readArtist("Ailee").getName());
	}

	@Test
	void testDeleteAlbum() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		assertEquals("Ailee", impl.readAlbum("AMY", new Artist("Ailee")).getArtist().getName());
		assertEquals("AMY", impl.readAlbum("AMY", new Artist("Ailee")).getName());
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

	@Test
	void testDeleteMusic() {
		impl.insertArtist("Ailee");
		impl.insertAlbum("AMY",new Artist("Ailee"));
		impl.insertMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		assertEquals("Dont teach me", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getName());
		assertEquals("AMY", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getAlbum().getName());
		assertEquals("Ailee", impl.readMusic("Dont teach me",new Artist("Ailee"),new Album("AMY", new Artist("Ailee"))).getAlbum().getArtist().getName());
		impl.deleteMusic("Dont teach me", new Artist("Ailee"), new Album("AMY", new Artist("Ailee")));
		impl.deleteAlbum("AMY",new Artist("Ailee"));
		impl.deleteArtist("Ailee");
	}

}
