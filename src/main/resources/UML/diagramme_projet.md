@startuml
allowmixing
package ViewModel{
	class ViewModel
}
package Model{
	class Music
	class Artist
	class Album
}

folder View{
	package fxmlcontroller{
		class FXMLController
		class FXMLMusicView
		class FXMLArtistView
		class FXMLAlbumView
	}
	package Modal{
		class modalModify
		class modalAdd
		class modalSystem
	}
	class AppView
}
package Bdd{
	class SQLImpl
	class SQLJDBC
}

@enduml