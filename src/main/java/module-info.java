module fr.ouk {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.media;
	requires java.logging;
	requires java.sql;
	requires org.xerial.sqlitejdbc;
	requires org.apache.commons.io;
	requires javafx.graphics;
	requires javafx.base;
    opens fr.ouk.ronindisco to javafx.fxml;
    exports fr.ouk.ronindisco;
    opens fr.ouk.ronindisco.view to javafx.graphics;
    exports fr.ouk.ronindisco.view;
    opens fr.ouk.ronindisco.view.fxmlcontroller to  javafx.fxml;
    exports fr.ouk.ronindisco.view.fxmlcontroller ;
    exports fr.ouk.ronindisco.view.fxmlcontroller.tabview to javafx.fxml;
    opens fr.ouk.ronindisco.view.fxmlcontroller.tabview to javafx.fxml;
    opens fr.ouk.ronindisco.model to javafx.base;
    exports fr.ouk.ronindisco.view.fxmlcontroller.modal to javafx.fxml;
    opens fr.ouk.ronindisco.view.fxmlcontroller.modal to  javafx.fxml;
}