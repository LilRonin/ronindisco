package fr.ouk.ronindisco.view;

import java.io.IOException;

import fr.ouk.ronindisco.utils.Utils;
import fr.ouk.ronindisco.view.fxmlcontroller.FXMLController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppView extends Application {
    private static Stage stage;

    @Override
    public void start(@SuppressWarnings("exports") Stage s) throws IOException {
        stage=s;
        setRoot("principal","RoninDisco");
    }

    static void setRoot(String fxml) throws IOException {
        setRoot(fxml,stage.getTitle());
    }

    static void setRoot(String fxml, String title) throws IOException {
        Scene scene = new Scene(loadFXML(fxml));
        stage.setTitle(title);
        stage.setScene(scene);
        stage.setOnCloseRequest(event-> System.exit(0));
        stage.show();
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Utils.getFxml(fxml));
        fxmlLoader.setController(new FXMLController());
        return fxmlLoader.load();
    }
}
