package fr.ouk.ronindisco.view.fxmlcontroller.modal;

import java.io.IOException;

import fr.ouk.ronindisco.utils.Utils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ModalSystem {
	private String message;
	private Stage dialog;
	
	
	public ModalSystem(String message) {
		this.message = message;
	}


	public void setModality() {
		dialog.initModality(Modality.APPLICATION_MODAL);
	}
	
	
	public void show() {
		dialog= new Stage();
		setModality();
		FXMLLoader loader =new FXMLLoader(Utils.getFxml("system"));
		try {
			dialog.setScene(new Scene(loader.load()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		dialog.getScene().setUserData(loader);
		loader = (FXMLLoader) dialog.getScene().getUserData();
		ModalSystemController mac =(ModalSystemController)(loader.getController());
		mac.getOkBtn().setOnAction(event -> dialog.close());
		mac.getMessage().setText(message);
		dialog.setResizable(false);
		dialog.setTitle("System");

		dialog.show();
	}
}
