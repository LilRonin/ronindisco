package fr.ouk.ronindisco.view.fxmlcontroller.modal;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class ModalConfirmController implements Initializable {
	@FXML
	private Button confirm;
	@FXML
	private Button cancel;
	@FXML
	private Text text;
	@FXML
	private TextField field;
	




	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}
	public TextField getField() {
		return field;
	}


	public void setField(TextField field) {
		this.field = field;
	}

	public Button getConfirm() {
		return confirm;
	}


	public void setConfirm(Button confirm) {
		this.confirm = confirm;
	}


	public Button getCancel() {
		return cancel;
	}


	public void setCancel(Button cancel) {
		this.cancel = cancel;
	}


	public Text getText() {
		return text;
	}


	public void setText(Text text) {
		this.text = text;
	}

}
