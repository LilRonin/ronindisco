package fr.ouk.ronindisco.view.fxmlcontroller.modal;

import java.io.IOException;

import fr.ouk.ronindisco.model.Album;
import fr.ouk.ronindisco.model.Artist;
import fr.ouk.ronindisco.model.Music;
import fr.ouk.ronindisco.utils.Utils;
import fr.ouk.ronindisco.viewmodel.DiscoViewModel;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class  ModalAdd{
	private String type;
	private Stage dialog;

	//défini sur NONE pour ne pas bloqué la fenêtre en arriere plan
	public void setModality() {
		dialog.initModality(Modality.APPLICATION_MODAL);
	}
	

	/**
	 * Type is the of object that will be add
	 * @param type
	 */
	public ModalAdd(String type) {
		this.type = type;
	}



	public void show() {
		dialog= new Stage();
		setModality();
		FXMLLoader loader =new FXMLLoader(Utils.getFxml("add"));
		try {
			dialog.setScene(new Scene(loader.load()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		dialog.getScene().setUserData(loader);
		loader = (FXMLLoader) dialog.getScene().getUserData();
		ModalAddController mac =(ModalAddController)(loader.getController());
		mac.getCancel().setOnAction(event -> dialog.close());
		dialog.setResizable(false);
		dialog.setTitle("System");
		build(mac);
		dialog.show();
	}

	/**
	 * Build the formula
	 * @param mac
	 */
	public void build(ModalAddController mac) {
		switch (this.type) {
		case "artist": {
			mac.getAlbumPane().setDisable(true);
			mac.getArtistPane().setDisable(true);
			mac.getConfirm().setOnAction(event-> confirmArtist(mac,dialog));
			
		}break;
		case "album": {
			mac.getAlbumPane().setDisable(true);
			mac.getArtistList().setItems(DiscoViewModel.getInstance().getListA());
			mac.getConfirm().setOnAction(event-> confirmAlbum(mac,dialog,mac.getArtistList()));
			mac.getAddArtist().setOnAction(event->{
				ModalAdd modal = new ModalAdd("artist");
				modal.show();
			} );
			}
		break;
		case "music": {
			mac.getArtistList().setItems(DiscoViewModel.getInstance().getListA());
			mac.getAlbumList().setItems(DiscoViewModel.getInstance().getListAm());
			mac.getConfirm().setOnAction(event-> confirmMusic(mac,dialog,mac.getArtistList(), mac.getAlbumList()));
			mac.getAddArtist().setOnAction(event->{
				ModalAdd modal = new ModalAdd("artist");
				modal.show();
			});
			mac.getAddAlbum().setOnAction(event->{
				ModalAdd modal = new ModalAdd("album");
				modal.show();
			});
		}
		break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + type);
		}
	}
	/**
	 * Method that ensure that the field are fill and correct for database while insert music
	 * @param mac
	 * @param stage
	 * @param artist
	 * @param album
	 */
	private void confirmMusic(ModalAddController mac, Stage stage, ComboBox<Artist> artist, ComboBox<Album> album) {
		if (!isNameEmpty(mac.getNameText())&& !isArtistEmpty(artist).get() && !isAlbumEmpty(album).get()) {
			if(isConfirmableMusic(mac.getNameText(),artist.getValue(),album.getValue())) {
				if(!isConfirmableAlbum(new TextField(album.getValue().getName()),artist.getValue())) {
					DiscoViewModel.getInstance().getImpl().insertMusic(mac.getNameText().getText(),artist.getValue(),album.getValue());
					DiscoViewModel.getInstance().updateMusicList();
				stage.close();
				}else mac.getTextInfo().setText("Album and artist does not match !");
			}else mac.getTextInfo().setText("Entry already exist in database !");
		}else mac.getTextInfo().setText("Empty field(s) !");
	}

	/**
	 * Method that ensure that the field are fill and correct for database while insert album
	 * @param mac
	 * @param stage
	 * @param artist
	 */
	private void confirmAlbum(ModalAddController mac, Stage stage, ComboBox<Artist> artist) {
		if (!isNameEmpty(mac.getNameText())&& !isArtistEmpty(artist).get()) {
			if(isConfirmableAlbum(mac.getNameText(),artist.getValue())) {
				DiscoViewModel.getInstance().getImpl().insertAlbum(mac.getNameText().getText(),artist.getValue());
				DiscoViewModel.getInstance().updateAlbumList();
				stage.close();
			}else mac.getTextInfo().setText("Entry already exist in database !");
		}else mac.getTextInfo().setText("Empty field(s) !");
	
	}
	/**
	 * Method that ensure that the field are fill and correct for database while insert artist
	 * @param mac
	 * @param stage
	 */
	public void confirmArtist(ModalAddController mac, Stage stage) {
		if (!isNameEmpty(mac.getNameText())) {
			if(isConfirmableArtist(mac.getNameText())) {
				DiscoViewModel.getInstance().getImpl().insertArtist(mac.getNameText().getText());
				DiscoViewModel.getInstance().updateArtistList();
				stage.close();
			}else mac.getTextInfo().setText("Entry already exist in database !");
		}else mac.getTextInfo().setText("Empty field(s) !");
	}

	/**
	 * Ensure that the combobox for artist is used
	 * @param artist
	 * @return
	 */
	public BooleanBinding isArtistEmpty(ComboBox<Artist> artist) {
		return artist.valueProperty().isNull();
	}
	/**
	 * Ensure that the combobox for album is used
	 * @param artist
	 * @return
	 */
	public BooleanBinding isAlbumEmpty(ComboBox<Album> album) {
		return album.valueProperty().isNull();
	}
	/**
	 * Text field empty
	 * @param name
	 * @return
	 */
	public boolean isNameEmpty(TextField name) {
		return name.getText().isBlank();
	}
	/**
	 * Is the artist already in databse
	 * @param name
	 * @return
	 */
	public boolean isConfirmableArtist(TextField name) {
			for (Artist artist : DiscoViewModel.getInstance().getListA()) {
				if (artist.getName().equals(name.getText())) {
					return false;
				}
			}
		return true;
		
	}
	/**
	 * Is the album already in database
	 * @param name
	 * @param artist
	 * @return
	 */
	public boolean isConfirmableAlbum(TextField name, Artist artist) {
		for (Album album : DiscoViewModel.getInstance().getListAm()) {
			if (album.getName().equals(name.getText()) && artist.getName().equals(album.getArtistName())) {
				return false;
			}
		}
	return true;
}
	/**
	 * Is the music already in database
	 * @param name
	 * @param artist
	 * @param album
	 * @return
	 */
	public boolean isConfirmableMusic(TextField name, Artist artist, Album album) {
		for (Music music : DiscoViewModel.getInstance().getListM()) {
			if (music.getName().equals(name.getText()) && artist.getName().equals(music.getAlbum().getArtist().getName()) && album.getName().equals(music.getAlbum().getName())) {
				return false;
			}
		}
	return true;
}



	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Stage getDialog() {
		return dialog;
	}


	public void setDialog(Stage dialog) {
		this.dialog = dialog;
	}

	
}
