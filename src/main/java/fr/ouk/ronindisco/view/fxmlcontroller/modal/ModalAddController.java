package fr.ouk.ronindisco.view.fxmlcontroller.modal;

import java.net.URL;
import java.util.ResourceBundle;

import fr.ouk.ronindisco.model.Album;
import fr.ouk.ronindisco.model.Artist;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class ModalAddController implements Initializable{

	@FXML
	private Pane namePane;
	@FXML
	private Pane albumPane;
	@FXML
	private Pane artistPane;
	@FXML
	private Button cancel;
	@FXML
	private Button confirm;
	@FXML
	private Button addAlbum;
	@FXML
	private Button addArtist;
	@FXML
	private TextField nameText;
	@FXML
	private ComboBox<Artist> artistList;
	@FXML
	private ComboBox<Album> albumList;
	@FXML
	private Text textInfo;

	public Pane getNamePane() {
		return namePane;
	}

	public void setNamePane(Pane namePane) {
		this.namePane = namePane;
	}

	public Pane getAlbumPane() {
		return albumPane;
	}

	public void setAlbumPane(Pane albumPane) {
		this.albumPane = albumPane;
	}

	public Pane getArtistPane() {
		return artistPane;
	}

	public void setArtistPane(Pane artistPane) {
		this.artistPane = artistPane;
	}

	public Button getCancel() {
		return cancel;
	}

	public void setCancel(Button cancel) {
		this.cancel = cancel;
	}

	public Button getConfirm() {
		return confirm;
	}

	public void setConfirm(Button confirm) {
		this.confirm = confirm;
	}

	public Button getAddAlbum() {
		return addAlbum;
	}

	public void setAddAlbum(Button addAlbum) {
		this.addAlbum = addAlbum;
	}

	public Button getAddArtist() {
		return addArtist;
	}

	public void setAddArtist(Button addArtist) {
		this.addArtist = addArtist;
	}

	public TextField getNameText() {
		return nameText;
	}

	public void setNameText(TextField nameText) {
		this.nameText = nameText;
	}

	public ComboBox<Artist> getArtistList() {
		return artistList;
	}

	public void setArtistList(ComboBox<Artist> artistList) {
		this.artistList = artistList;
	}

	public ComboBox<Album> getAlbumList() {
		return albumList;
	}

	public void setAlbumList(ComboBox<Album> albumList) {
		this.albumList = albumList;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

	public Text getTextInfo() {
		return textInfo;
	}

	public void setTextInfo(Text textInfo) {
		this.textInfo = textInfo;
	}

}
