package fr.ouk.ronindisco.view.fxmlcontroller.tabview;

import java.net.URL;
import java.util.ResourceBundle;

import fr.ouk.ronindisco.model.Album;
import fr.ouk.ronindisco.model.Artist;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalAdd;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalDelete;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalSystem;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalUpdate;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;

public class TabViewAlbumController implements Initializable, TableViewInterface {
	
	@FXML
	private TextField search;
	@FXML
	private TableView<Album> tableView;
	@FXML
	private TableColumn<Artist, String> artist;
	@FXML
	private TableColumn<Album, String> album;
	@FXML
	private Button add;
	@FXML
	private Button delete;
	@FXML
	private Button modify;
	
	private Album selectedAlbum;
	
	
	
	//https://www.youtube.com/watch?v=2M0L6w3tMOY
		public void fillTab(ObservableList<Album> list){
			/*for (Album album : list) {
				obsAlbum.add(album);
			}*/
			artist.setCellValueFactory(new PropertyValueFactory<>("artistName"));
			album.setCellValueFactory(new PropertyValueFactory<>("name"));
			tableView.setItems(list);
			FilteredList<Album> filteredData=new FilteredList<>(list, b-> true);
			search.textProperty().addListener((observable,oldValue,newValue) ->{
				filteredData.setPredicate(album -> {
					if (newValue.isEmpty()||newValue.isBlank()||newValue==null) {
						return true;
					}
					String searchValue=newValue.toLowerCase();
					if(album.getName().toLowerCase().indexOf(searchValue)> -1) {
						return true;
					}else if(album.getArtist().getName().toLowerCase().indexOf(searchValue)> -1) {
							return true;
							}else {return false;}
				});
			});
			SortedList<Album> sortedData= new SortedList<>(filteredData);
			sortedData.comparatorProperty().bind(tableView.comparatorProperty());
			tableView.setItems(sortedData);
			
			tableView.setOnMouseClicked(event -> {
		        if(event.getButton().equals(MouseButton.PRIMARY)){
		            selectedAlbum=tableView.getSelectionModel().getSelectedItem();
		        }
		    });
		}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		btnAdd();
		btnDelete();
		btnUpdate();
	}
	public void btnAdd(){
		add.setOnAction(event->add());
	}
	@Override
	public void add() {
		ModalAdd modal= new ModalAdd("album");
		modal.show();
		
	}

	public void btnDelete(){
		delete.setOnAction(event->delete());
	}

	@Override
	public void delete() {
		if (selectedAlbum==null) {
			ModalSystem ms= new ModalSystem("Please select an element");
			ms.show();
		}else {
			ModalDelete mc = new ModalDelete("Are you sure?",selectedAlbum);
			mc.show();
		}
		
	}

	public void btnUpdate(){
		modify.setOnAction(event->update());
	}
	@Override
	public void update() {
		if (selectedAlbum==null) {
			ModalSystem ms= new ModalSystem("Please select an element");
			ms.show();
		}else {
			ModalUpdate mu = new ModalUpdate(selectedAlbum);
			mu.show();
		}
		
	}


}
