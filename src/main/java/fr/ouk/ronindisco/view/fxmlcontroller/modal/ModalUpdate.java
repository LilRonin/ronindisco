package fr.ouk.ronindisco.view.fxmlcontroller.modal;

import java.io.IOException;

import fr.ouk.ronindisco.model.Album;
import fr.ouk.ronindisco.model.Artist;
import fr.ouk.ronindisco.model.Element;
import fr.ouk.ronindisco.model.Music;
import fr.ouk.ronindisco.utils.Utils;
import fr.ouk.ronindisco.viewmodel.DiscoViewModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ModalUpdate {
	private Element element;
	private Stage dialog;
	
	
	public ModalUpdate(Element element) {
		this.element = element;
	}
	public void setModality() {
		dialog.initModality(Modality.APPLICATION_MODAL);
	}
	public void show() {
		dialog= new Stage();
		setModality();
		FXMLLoader loader =new FXMLLoader(Utils.getFxml("confirm"));
		try {
			dialog.setScene(new Scene(loader.load()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		dialog.getScene().setUserData(loader);
		loader = (FXMLLoader) dialog.getScene().getUserData();
		ModalConfirmController mac =(ModalConfirmController)(loader.getController());
		mac.getText().setVisible(false);
		mac.getCancel().setOnAction(event -> dialog.close());
		mac.getConfirm().setOnAction(event-> confirm(mac));
		dialog.setResizable(false);
		dialog.setTitle("System");
		dialog.show();
	}
	/**
	 * Confirm update
	 * @param mac
	 */
	private void confirm(ModalConfirmController mac) {
		if (!mac.getField().getText().isBlank()) {
			if (element.getClass().equals(Artist.class)) {
				DiscoViewModel.getInstance().getImpl().updateArtist(mac.getField().getText(), ((Artist) element));
			} else if (element.getClass().equals(Album.class)) {
				DiscoViewModel.getInstance().getImpl().updateAlbum(mac.getField().getText(), ((Album) element));
			} else if (element.getClass().equals(Music.class)) {
				DiscoViewModel.getInstance().getImpl().updateMusic(mac.getField().getText(), ((Music) element));
			}
			DiscoViewModel.getInstance().updateAlbumList();
			DiscoViewModel.getInstance().updateArtistList();
			DiscoViewModel.getInstance().updateMusicList();
			dialog.close();
		} else {
			dialog.close();
		}

	}
}
