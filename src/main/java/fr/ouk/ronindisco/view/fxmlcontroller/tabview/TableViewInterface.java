package fr.ouk.ronindisco.view.fxmlcontroller.tabview;

public interface TableViewInterface {
	public void add();
	public void delete();
	public void update();
}
