package fr.ouk.ronindisco.view.fxmlcontroller;
/*
Put header here


 */

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import fr.ouk.ronindisco.utils.Utils;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalAdd;
import fr.ouk.ronindisco.view.fxmlcontroller.tabview.TabViewAlbumController;
import fr.ouk.ronindisco.view.fxmlcontroller.tabview.TabViewArtistController;
import fr.ouk.ronindisco.view.fxmlcontroller.tabview.TabViewMusicController;
import fr.ouk.ronindisco.viewmodel.DiscoViewModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

public class FXMLController implements Initializable {
	@FXML
	private MenuItem btnDbConnect;
	@FXML
	private MenuItem btnDbDisconnect;
	@FXML
	private MenuItem menuAddArtist;
	@FXML
	private MenuItem menuAddMusic;
	@FXML
	private MenuItem menuAddAlbum;
	@FXML
	private Button buttonTest;
	@FXML
	private AnchorPane anchorPane;
	@FXML
	private Pane pane;
	@FXML
	private AnchorPane musicPane;
	@FXML
	private AnchorPane albumPane;
	@FXML
	private AnchorPane artistPane;
	@FXML
	private Button mainButton;
	@FXML
	private TabPane tabPane;



	/**
	 * Method that load the music list
	 */
	private void loadMusicPane() {
		try {
			FXMLLoader loader = new FXMLLoader(Utils.getFxml("tabMusic"));
			Pane pane2 = loader.load();
			pane2.setUserData(loader);
			loader = (FXMLLoader) pane2.getUserData();
			TabViewMusicController tvmc = (TabViewMusicController) loader.getController();
			musicPane.getChildren().clear();
			musicPane.getChildren().setAll(pane2);
			tvmc.fillTab(DiscoViewModel.getInstance().getListM());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Method that load the album list
	 */
	private void loadAlbumPane() {
		try {
			FXMLLoader loader = new FXMLLoader(Utils.getFxml("tabAlbum"));
			Pane pane2 = loader.load();
			pane2.setUserData(loader);
			loader = (FXMLLoader) pane2.getUserData();
			TabViewAlbumController tvmc = (TabViewAlbumController) loader.getController();
			albumPane.getChildren().clear();
			albumPane.getChildren().setAll(pane2);
			tvmc.fillTab(DiscoViewModel.getInstance().getListAm());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Method that load the artist list
	 */
	private void loadArtistPane() {
		try {
			FXMLLoader loader = new FXMLLoader(Utils.getFxml("tabArtist"));
			Pane pane2 = loader.load();
			pane2.setUserData(loader);
			loader = (FXMLLoader) pane2.getUserData();
			TabViewArtistController tvmc = (TabViewArtistController) loader.getController();
			artistPane.getChildren().clear();
			artistPane.getChildren().setAll(pane2);
			tvmc.fillTab(DiscoViewModel.getInstance().getListA());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Method that load all lists
	 */
	private void loadPane() {
		loadAlbumPane();
		loadMusicPane();
		loadArtistPane();
		
	}
	/**
	 * Init add menu from main screen
	 */
	public void initAddMenu() {
		menuAddMusic.setOnAction(event->addMusic());
		menuAddArtist.setOnAction(event->addArtist());
		menuAddAlbum.setOnAction(event->addAlbum());
	}
	/**
	 * Add music
	 */
	public void addMusic() {
		ModalAdd modal= new ModalAdd("music");
		modal.show();
	}
	/**
	 * Add artist
	 */
	public void addArtist() {
		ModalAdd modal= new ModalAdd("artist");
		modal.show();
	}
	/**
	 * Add album
	 */
	public void addAlbum() {
		ModalAdd modal= new ModalAdd("album");
		modal.show();
	}
	/**
	 * Back to main menu
	 */
	private void mainButton() {
		mainButton.setOnAction(event-> loadPane());
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		mainButton();
		loadPane();
		initAddMenu();
	}
}
