package fr.ouk.ronindisco.view.fxmlcontroller.tabview;

import java.net.URL;
import java.util.ResourceBundle;

import fr.ouk.ronindisco.model.Artist;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalAdd;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalDelete;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalSystem;
import fr.ouk.ronindisco.view.fxmlcontroller.modal.ModalUpdate;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;

public class TabViewArtistController implements Initializable, TableViewInterface {
	
	@FXML
	private TextField search;
	@FXML
	private TableView<Artist> tableView;
	@FXML
	private TableColumn<Artist, String> artist;
	@FXML
	private Button add;
	@FXML
	private Button delete;
	@FXML
	private Button modify;

	private Artist selectedArtist;
	
	
	
	//https://www.youtube.com/watch?v=2M0L6w3tMOY
		public void fillTab(ObservableList<Artist> list){
			/*for (Artist artist : list) {
				obsArtist.add(artist);
			}*/
			artist.setCellValueFactory(new PropertyValueFactory<>("name"));
			tableView.setItems(list);
			FilteredList<Artist> filteredData=new FilteredList<>(list, b-> true);
			search.textProperty().addListener((observable,oldValue,newValue) ->{
				filteredData.setPredicate(artist -> {
					if (newValue.isEmpty()||newValue.isBlank()||newValue==null) {
						return true;
					}
					String searchValue=newValue.toLowerCase();
					if(artist.getName().toLowerCase().indexOf(searchValue)> -1) {
						return true;
					
							}else {return false;}
				});
			});
			SortedList<Artist> sortedData= new SortedList<>(filteredData);
			sortedData.comparatorProperty().bind(tableView.comparatorProperty());
			tableView.setItems(sortedData);
			
			tableView.setOnMouseClicked(event -> {
		        if(event.getButton().equals(MouseButton.PRIMARY)){
		            selectedArtist=tableView.getSelectionModel().getSelectedItem();
		        }
		    });
		}
		
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		btnAdd();
		btnDelete();
		btnUpdate();
	}
	public void btnAdd(){
		add.setOnAction(event->add());
	}

	@Override
	public void add() {
		ModalAdd modal= new ModalAdd("artist");
		modal.show();
	}
	public void btnDelete(){
		delete.setOnAction(event->delete());
	}

	@Override
	public void delete() {
		if (selectedArtist==null) {
			ModalSystem ms= new ModalSystem("Please select an element");
			ms.show();
		}else {
			ModalDelete mc = new ModalDelete("Are you sure?",selectedArtist);
			mc.show();
		}
		
	}

	public void btnUpdate(){
		modify.setOnAction(event->update());
	}
	@Override
	public void update() {
		if (selectedArtist==null) {
			ModalSystem ms= new ModalSystem("Please select an element");
			ms.show();
		}else {
			ModalUpdate mu = new ModalUpdate(selectedArtist);
			mu.show();
		}
		
	}

}
