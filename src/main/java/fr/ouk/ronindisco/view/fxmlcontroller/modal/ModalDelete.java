package fr.ouk.ronindisco.view.fxmlcontroller.modal;

import java.io.IOException;

import fr.ouk.ronindisco.model.Album;
import fr.ouk.ronindisco.model.Artist;
import fr.ouk.ronindisco.model.Element;
import fr.ouk.ronindisco.model.Music;
import fr.ouk.ronindisco.utils.Utils;
import fr.ouk.ronindisco.viewmodel.DiscoViewModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ModalDelete {
	private String message;
	private Element element;
	private Stage dialog;
	
	public void setModality() {
		dialog.initModality(Modality.APPLICATION_MODAL);
	}
	
	public ModalDelete(String message, Element element) {
		this.message = message;
		this.element = element;
	}
	public void show() {
		dialog= new Stage();
		setModality();
		FXMLLoader loader =new FXMLLoader(Utils.getFxml("confirm"));
		try {
			dialog.setScene(new Scene(loader.load()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		dialog.getScene().setUserData(loader);
		loader = (FXMLLoader) dialog.getScene().getUserData();
		ModalConfirmController mac =(ModalConfirmController)(loader.getController());
		mac.getField().setVisible(false);
		mac.getText().setText(message);
		mac.getCancel().setOnAction(event -> dialog.close());
		mac.getConfirm().setOnAction(event-> confirm());
		dialog.setResizable(false);
		dialog.setTitle("System");
		dialog.show();
	}
	/**
	 * Confirm delete
	 */
	private void confirm() {
		if (element.getClass().equals(Artist.class)) {
			DiscoViewModel.getInstance().getImpl().deleteArtist(((Artist)element).getName());
		}else if (element.getClass().equals(Album.class)) {
			DiscoViewModel.getInstance().getImpl().deleteAlbum(((Album)element).getName(),((Album)element).getArtist());
		}else if (element.getClass().equals(Music.class)) {
			DiscoViewModel.getInstance().getImpl().deleteMusic(((Music)element).getName(),((Music)element).getAlbum().getArtist(),((Music)element).getAlbum());
		}
		DiscoViewModel.getInstance().updateAlbumList();
		DiscoViewModel.getInstance().updateArtistList();
		DiscoViewModel.getInstance().updateMusicList();
		dialog.close();
		
	}
}
