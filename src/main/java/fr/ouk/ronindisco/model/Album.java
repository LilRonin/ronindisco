package fr.ouk.ronindisco.model;

public class Album implements Element{
	private String name;
	private Artist artist;
	private String artistName;

	public Album() {
		
	}

	public Album(String name, Artist artist) {
		setName(name);
		setArtist(artist);
		this.artistName=getArtist().getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	@Override
	public String toString() {
		return String.join(" ", getName(),"by", getArtistName());
	}
	

}
