package fr.ouk.ronindisco.model;

public class Artist implements Element{
	private String name;
	
	public Artist() {
	}

	public Artist(String name) {
		setName(name);
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return getName();
	}

}
