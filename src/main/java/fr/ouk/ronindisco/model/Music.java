package fr.ouk.ronindisco.model;

public class Music implements Element{
	private String name;
	private Album album;
	private String albumName;
	private String artistName;
	
	public Music() {

	}
	public Music(String name, Album album) {
		setName(name);
		setAlbum(album);
		this.albumName= album.getName();
		this.artistName=album.getArtist().getName();
	}

	public String getAlbumName() {
		return albumName;
	}
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}
	

}
