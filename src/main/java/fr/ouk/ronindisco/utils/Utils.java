package fr.ouk.ronindisco.utils;

import java.net.URL;

import fr.ouk.ronindisco.MainApp;

public interface Utils {
	/**
	 * Utilitaire pour recuperer la bdd
	 * @return
	 */
	public static String getDb() {
		//String str=MainApp.class.getResource("/resources/database/").getPath();
		String str=MainApp.class.getResource("/database/point.txt").getPath();
		
		str=str.substring(0, str.indexOf("/point.txt"));
		System.out.println(str);
		//TODO str 
		return str;
	}
	/**
	 * utilitaire pour charger les fxml
	 * @param fxmlName
	 * @return
	 */
	//les ligne en commentaire sont a utilisé pour faire tourner sur l'ide et l'autre sur le JAR
	public static URL getFxml(String fxmlName) {
		//return MainApp.class.getResource("/resources/fxml/"+fxmlName + ".fxml");
		return MainApp.class.getResource("/fxml/"+fxmlName + ".fxml");
	}

}
