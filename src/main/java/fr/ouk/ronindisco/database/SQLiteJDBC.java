package fr.ouk.ronindisco.database;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.sqlite.SQLiteConfig;

import fr.ouk.ronindisco.utils.Utils;

/**
 * Class that manage the database
 * @author shuly
 *
 */
public class SQLiteJDBC {
		private static final Logger logger = Logger.getLogger("SQLiteJDBC");
		private Connection c ;
		private Properties prop;

		/**
		 * The file disco.properties is used to inject database name
		 */
		public SQLiteJDBC() {
			prop = new Properties();
			try (InputStream fic = this.getClass().getClassLoader().getResourceAsStream("disco.properties")) {
				prop.load(fic);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		/**
		 * Method that connect to the database if it exist and make a save from this.
		 * If it doesn't exist it create one with the name choosed in disco.properties under "bdd"
		 * @param bddChoice
		 */
		public void connect(String bddChoice) {
			String choix = prop.getProperty(bddChoice);
			String bdd=String.join("", "/",choix,".db");
			String bddold=String.join("","/",choix,"_old",".db");
			try {
				SQLiteConfig config = new SQLiteConfig();
				config.enforceForeignKeys(true); 
		        Class.forName("org.sqlite.JDBC");
		        File sav= new File(Utils.getDb()+bdd);
		        //sécurité de sauvegarde
		        if (!sav.exists()&&new File(Utils.getDb()+bddold).exists()) {
		        	FileUtils.copyFile(new File(Utils.getDb()+bddold),sav);
		        	logger.log(Level.INFO,"La base de donnée n'a pas été trouver => chargement de la précédente");
				}
		        if(sav.exists()) {
		        	FileUtils.copyFile(sav, new File(Utils.getDb()+bddold));
		        	logger.log(Level.INFO,"La base de donnée est connectée création d'une version anterieure");
		        }
		         c = DriverManager.getConnection("jdbc:sqlite:"+Utils.getDb()+bdd,config.toProperties());
		         logger.log(Level.INFO,"Base de donnée connectée");
			} catch (Exception e) {
				logger.log(Level.WARNING,e.getMessage());
				
			}
		}
		/**
		 * Disconnect from the current database
		 */
		public void closeConnect() {
			try {
				getC().close();
				logger.log(Level.INFO,"Base de données déconnectée");
			} catch (SQLException e) {
				logger.log(Level.WARNING,e.getMessage());
			}
		}

		public Connection getC() {
			return c;
		}

		public void setC(Connection c) {
			this.c = c;
		}
		
	}

