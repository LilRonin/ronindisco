package fr.ouk.ronindisco.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.ouk.ronindisco.model.Album;
import fr.ouk.ronindisco.model.Artist;
import fr.ouk.ronindisco.model.Music;


/**
 * Class that query the database
 * @author ronin
 *
 */

public class SQLiteImpl {
		private static final Logger logger = Logger.getLogger("SQLiteImpl");
		private SQLiteJDBC connect;

		public SQLiteImpl(SQLiteJDBC connect) {
			this.connect = connect;
		}
		/**
		 * Create table in database with all constraints
		 */
		public void createTable() {
	        PreparedStatement ps=null;
	        try {
	        	String sql= String.join("","PRAGMA foreign_keys = ON;");
				ps = connect.getC().prepareStatement(sql);
				ps.executeUpdate();
	            sql= String.join("","CREATE TABLE IF NOT EXISTS Artist",
	            		"(NAME VARCHAR(30) NOT NULL PRIMARY KEY);");
				ps = connect.getC().prepareStatement(sql);
				ps.executeUpdate();
				sql= String.join("", "CREATE TABLE IF NOT EXISTS Album ", 
	           		 "(NAME VARCHAR(30) NOT NULL,",
	           		 "ARTIST VARCHAR(30) NOT NULL,",
	           		 "PRIMARY KEY(NAME,ARTIST)",
	           		 "CONSTRAINT fk_artist FOREIGN KEY(ARTIST) REFERENCES Artist(NAME)",
	           		 "ON DELETE CASCADE ",
	           		 "ON UPDATE CASCADE);");
				ps = connect.getC().prepareStatement(sql);
				ps.executeUpdate();
				sql= String.join("", "CREATE TABLE IF NOT EXISTS Music ", 
		           		 "(NAME VARCHAR(30) NOT NULL,",
		           		 "ALBUM VARCHAR(30) NOT NULL,",
		           		 "ARTIST VARCHAR(30) NOT NULL,",
		           		"PRIMARY KEY(NAME,ALBUM,ARTIST)",
		           		 "CONSTRAINT fk_album FOREIGN KEY(ALBUM,ARTIST) REFERENCES Album(NAME,ARTIST)",
		           		 "ON DELETE CASCADE ",
		           		 "ON UPDATE CASCADE);");
					ps = connect.getC().prepareStatement(sql);
					ps.executeUpdate();
			} catch (SQLException e) {
				logger.log(Level.WARNING,e.getMessage());
			}
		}
		/**
		 * Method that insert an artist in database
		 * @param name
		 */
		public void insertArtist(String name) {
		       PreparedStatement ps=null;
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "INSERT INTO Artist (NAME)",
		            		"VALUES ('",name,"');"); 
					ps = connect.getC().prepareStatement(sql);
					ps.executeUpdate();
					connect.getC().commit();
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}
		}
		/**
		 * Method that update the artist name
		 * @param name
		 * @param artist
		 */
		public void updateArtist(String name, Artist artist) {
		       PreparedStatement ps=null;
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "UPDATE Artist SET NAME='",name,"' WHERE NAME='",artist.getName(),"';"); 
					ps = connect.getC().prepareStatement(sql);
					ps.executeUpdate();
					connect.getC().commit();
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}
		}
		/**
		 * Method that update album name only
		 * @param name
		 * @param album
		 */
		public void updateAlbum(String name, Album album) {
		       PreparedStatement ps=null;
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "UPDATE Album SET NAME='",name,"', ARTIST='",album.getArtist().getName(),"' WHERE NAME='",album.getName(),"'AND ARTIST='",album.getArtist().getName(),"';"); 
					ps = connect.getC().prepareStatement(sql);
					ps.executeUpdate();
					connect.getC().commit();
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}
		}
		/**
		 * Method that insert an album in database
		 * @param name
		 * @param artist
		 */
		public void insertAlbum(String name, Artist artist) {
		       PreparedStatement ps=null;
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "INSERT INTO Album (NAME,ARTIST)",
		            		"VALUES (?,?);"); 
					ps = connect.getC().prepareStatement(sql);
					ps.setString(1, name);
					ps.setString(2, artist.getName());
					ps.executeUpdate();
					connect.getC().commit();
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}
		}
		/**
		 * Method that insert a music in database
		 * @param name
		 * @param artist
		 * @param album
		 */
		public void insertMusic(String name, Artist artist, Album album) {
		       PreparedStatement ps=null;
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "INSERT INTO Music (NAME,ARTIST,ALBUM)",
		            		"VALUES (?,?,?);"); 
					ps = connect.getC().prepareStatement(sql);
					ps.setString(1, name);
					ps.setString(2, artist.getName());
					ps.setString(3, album.getName());
					ps.executeUpdate();
					connect.getC().commit();
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}
		}
		/**
		 * Method that update music name only
		 * @param name
		 * @param music
		 */
		public void updateMusic(String name,Music music) {
		       PreparedStatement ps=null;
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "UPDATE Music SET NAME='",name,"', ARTIST='",music.getAlbum().getArtist().getName(),"',ALBUM='",music.getAlbum().getName(),"' WHERE NAME='",music.getName(),"'AND ARTIST='",music.getAlbum().getArtist().getName(),"' AND ALBUM='",music.getAlbum().getName(),"';"); 
					ps = connect.getC().prepareStatement(sql);
					ps.executeUpdate();
					connect.getC().commit();
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}
		}
		/**
		 * Method that read an artist in database
		 * @param name
		 * @return
		 */
		public Artist readArtist(String name) {
		       PreparedStatement ps=null;
		       ResultSet rs= null;
		       Artist artist= new Artist();
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "SELECT * FROM Artist WHERE NAME='",name,"';"); 
					ps = connect.getC().prepareStatement(sql);
					rs=ps.executeQuery();
					   while ( rs.next() ) {
					         String  named = rs.getString("NAME");
					         artist= new Artist(named);
					      }
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}return artist;
		}
		/**
		 * Method that read an album in database
		 * @param name
		 * @param artist
		 * @return
		 */
		public Album readAlbum(String name, Artist artist) {
		       PreparedStatement ps=null;
		       ResultSet rs= null;
		       Album album = new Album();
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "SELECT * FROM Album WHERE NAME='",name,"' AND ARTIST='",artist.getName(),"';"); 
					ps = connect.getC().prepareStatement(sql);
					rs=ps.executeQuery();
					if (rs!=null) {
					   while ( rs.next() ) {
					         String  nameResult = rs.getString("NAME");
					         Artist artistResult=new Artist(rs.getString("ARTIST"));
					         album=new Album(nameResult,artistResult);
					      }}
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}
		        return album;
		}
		/**
		 * Method that read a music in database
		 * @param name
		 * @param artist
		 * @param album
		 * @return
		 */
		public Music readMusic(String name, Artist artist,Album album) {
		       PreparedStatement ps=null;
		       ResultSet rs= null;
		       Music music = new Music();
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "SELECT * FROM Music WHERE NAME='",name,"'AND ARTIST='",artist.getName(),"' AND ALBUM='",album.getName(),"';"); 
					ps = connect.getC().prepareStatement(sql);
					rs=ps.executeQuery();
					if (rs!=null) {
					   while ( rs.next() ) {
					         String  nameResult = rs.getString("NAME");
					         Artist artistResult = new Artist(rs.getString("ARTIST"));
					         Album albumResult = new Album(rs.getString("ALBUM"), artistResult);
					         music = new Music(nameResult, albumResult);
					      }}
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}return music;
		}
		/**
		 * Method that read all the database artist entry and return a list
		 * @return
		 */
		public List<Artist> readAllArtist() {
		       PreparedStatement ps=null;
		       ResultSet rs= null;
		       List<Artist> list= new ArrayList<Artist>();
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "SELECT * FROM Artist;"); 
					ps = connect.getC().prepareStatement(sql);
					rs=ps.executeQuery();
					if (rs!=null) {
					   while ( rs.next() ) {
					         String  name = rs.getString("NAME");
					         Artist artist = new Artist(name);
					         list.add(artist);
					      }
					   }
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}return list;
		}
		/**
		 * Method that read all the database album entry and return a list
		 * @return
		 */
		public List<Album> readAllAlbum() {
		       PreparedStatement ps=null;
		       ResultSet rs= null;
		       List<Album> list= new ArrayList<Album>();	        
		       try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "SELECT * FROM Album;"); 
					ps = connect.getC().prepareStatement(sql);
					rs=ps.executeQuery();
					if (rs!=null) {
					   while ( rs.next() ) {
					         String  name = rs.getString("NAME");
					         Artist artist = new Artist(rs.getString("ARTIST"));
					         Album album = new Album(name, artist);
					         list.add(album);
					      }}
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}return list;
				}
		/**
		 * Method that read all the database music entry and return a list
		 * @return
		 */
		public List<Music> readAllMusic() {
		       PreparedStatement ps=null;
		       ResultSet rs= null;
		       List<Music> list= new ArrayList<Music>();	        
		       try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "SELECT * FROM Music;"); 
					ps = connect.getC().prepareStatement(sql);
					rs=ps.executeQuery();
					if (rs!=null) {
					   while ( rs.next() ) {
					         String  nameResult = rs.getString("NAME");
					         Artist artistResult = new Artist(rs.getString("ARTIST"));
					         Album albumResult = new Album(rs.getString("ALBUM"), artistResult);
					         Music music = new Music(nameResult, albumResult);
					         list.add(music);
					      }}
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}return list;
				}
		/**
		 * Method that delete an artist using name
		 * @param name
		 * @return
		 */
		public boolean deleteArtist(String name) {
		       PreparedStatement ps=null;
		       boolean done=false;
		        try {
		        	connect.getC().setAutoCommit(false);
		            String sql= String.join("", "DELETE from Artist WHERE NAME='",name,"';"); 
					ps = connect.getC().prepareStatement(sql);
					ps.executeUpdate();
					connect.getC().commit();
					done=true;
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}return done;
		}
		/**
		 * Method that delete album using name and artist
		 * @param name
		 * @param artist
		 * @return
		 */
		public boolean deleteAlbum(String name, Artist artist) {
		       PreparedStatement ps=null;
		       boolean done=false;
		        try {
		        	connect.getC().setAutoCommit(false);
					String sql= String.join("", "DELETE from Album WHERE NAME='",name,"' AND ARTIST='",artist.getName(),"';"); 
					ps = connect.getC().prepareStatement(sql);
					ps.executeUpdate();
					connect.getC().commit();
					done=true;
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}return done;
		}
		/**
		 * Method that delete a music using the name the artist and the album
		 * @param name
		 * @param artist
		 * @param album
		 * @return
		 */
		public boolean deleteMusic(String name, Artist artist,Album album) {
		       PreparedStatement ps=null;
		       boolean done=false;
		        try {
		        	connect.getC().setAutoCommit(false);
					String sql= String.join("", "DELETE from Music WHERE NAME='",name,"' AND ARTIST='",artist.getName(),"'AND ALBUM='",album.getName(),"';"); 
					ps = connect.getC().prepareStatement(sql);
					ps.executeUpdate();
					connect.getC().commit();
					done=true;
				} catch (SQLException e) {
					logger.log(Level.WARNING,e.getMessage());
				}return done;
		}
}
