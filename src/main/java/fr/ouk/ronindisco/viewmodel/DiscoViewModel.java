package fr.ouk.ronindisco.viewmodel;

import java.util.List;

import fr.ouk.ronindisco.database.SQLiteImpl;
import fr.ouk.ronindisco.database.SQLiteJDBC;
import fr.ouk.ronindisco.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

//TODO en faire un singleton
public class DiscoViewModel {
	private ObservableList<Music> listM;
	private ObservableList<Artist> listA;
	private ObservableList<Album> listAm;
	private SQLiteJDBC sqlJ;
	private SQLiteImpl impl;
	private static DiscoViewModel instance= new DiscoViewModel();
	
	private DiscoViewModel() {
		this.sqlJ= new SQLiteJDBC();
		sqlJ.connect("bdd");
		this.impl= new SQLiteImpl(sqlJ);
		impl.createTable();
		initiateMusicList();
		initiateAlbumtList();
		initiateArtistList();
		
	}
	

	public static DiscoViewModel getInstance() {
		return instance;
	}

	public static void setInstance(DiscoViewModel instance) {
		DiscoViewModel.instance = instance;
	}
	public void initiateMusicList() {
		listM= FXCollections.observableArrayList();
		for (Music music : impl.readAllMusic()) {
			listM.add(music);
		};
	}
	public void initiateArtistList() {
		listA= FXCollections.observableArrayList();
		for (Artist artist : impl.readAllArtist()) {
			listA.add(artist);
		};
	}
	public void initiateAlbumtList() {
		listAm= FXCollections.observableArrayList();
		for (Album album : impl.readAllAlbum()) {
			listAm.add(album);
		};
	}
	public void updateArtistList() {
		listA.clear();
		for (Artist artist : impl.readAllArtist()) {
			listA.add(artist);
		};
	}
	public void updateAlbumList() {
		listAm.clear();
		for (Album album : impl.readAllAlbum()) {
			listAm.add(album);
		};
	}
	public void updateMusicList() {
		listM.clear();
		for (Music music : impl.readAllMusic()) {
			listM.add(music);
		};
	}
	public void updateAllList() {
		updateArtistList();
		updateAlbumList();
		updateMusicList();
	}



	public ObservableList<Music> getListM() {
		return listM;
	}


	public void setListM(ObservableList<Music> listM) {
		this.listM = listM;
	}


	public ObservableList<Album> getListAm() {
		return listAm;
	}


	public void setListAm(ObservableList<Album> listAm) {
		this.listAm = listAm;
	}


	public ObservableList<Artist> getListA() {
		return listA;
	}


	public void setListA(ObservableList<Artist> listA) {
		this.listA = listA;
	}



	public SQLiteJDBC getSqlJ() {
		return sqlJ;
	}

	public void setSqlJ(SQLiteJDBC sqlJ) {
		this.sqlJ = sqlJ;
	}

	public SQLiteImpl getImpl() {
		return impl;
	}

	public void setImpl(SQLiteImpl impl) {
		this.impl = impl;
	}

}
